#include <stdio.h>
#include <string.h>

#define SIZE 20
typedef enum { false, true } bool;

bool validarNombre(char *nombre);
bool validarEdad(int edad);

int main(){
    char nombre[SIZE];
    char apellido[SIZE];
    int edad;
    
    char nombreVieja[SIZE];
    char apellidoVieja[SIZE];
    int edadVieja=0;
   
    char nombreJoven[SIZE];
    char apellidoJoven[SIZE];
    int edadJoven=130; 
    
    int suma = 0;
    double promedio;

    for (int i=0; i < 10; i++){
        printf("\nPersona %d\n", i+1);

        do{
            printf("Ingrese su nombre: ");
            scanf("%s", nombre);
        }while(!validarNombre(nombre));
        
        do{
            printf("Ingrese su apellido: ");
            scanf("%s", apellido);
        }while(!validarNombre(apellido));
        
        do{
            printf("Ingrese su edad: ");
            scanf("%d", &edad);
        }while(!validarEdad(edad));

        if (edad <= edadJoven){
            edadJoven = edad;
            strncpy ( nombreJoven, nombre, SIZE );
            strncpy ( apellidoJoven, apellido, SIZE );
 
        }
        
        if (edad >= edadVieja){
            edadVieja = edad;
            strncpy ( nombreVieja, nombre, SIZE );
            strncpy ( apellidoVieja, apellido, SIZE );
        }

        suma += edad;       
    }
    
    promedio = (double)suma/10.0;
    
    printf("\nPromedio de edad: %.2f", promedio);
    printf("\nPersona mas vieja: %s %s, edad =  %d", nombreVieja, apellidoVieja, edadVieja);
    printf("\nPersona mas joven: %s %s, edad = %d", nombreJoven, nombreJoven, edadJoven);   
    
}



bool validarNombre(char *nombre){
    bool valor=true;
    if (nombre[0] < 'A' || nombre[0] > 'Z' ){
           
        valor = false;
    }
    int i = 1;
    do{
        if (nombre[i] < 'a' || nombre[i] > 'z' ){
            valor = false;
        }
        i++;
    }while(nombre[i] != '\0');
    
    return valor;
}

bool validarEdad(int edad){
    bool valor = true;
    if (edad <= 0 || edad > 130){
        valor = false;
    }
    return valor;
}
